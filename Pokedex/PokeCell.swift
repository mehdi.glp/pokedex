//
//  PokeCell.swift
//  Pokedex
//
//  Created by Mehdi Gilanpour on 7/3/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import UIKit

class PokeCell: UICollectionViewCell {
    
    @IBOutlet weak var PokeImage: UIImageView!
    @IBOutlet weak var PokeLabel: UILabel!
    
    var pokemon :Pokemon!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.cornerRadius = 5.0
    }
    
    func configureCell(pokemon : Pokemon){
        
        self.pokemon = pokemon
        PokeLabel.text = self.pokemon.name.capitalizedString
        PokeImage.image = UIImage(named: "\(self.pokemon.index)")
        
    }
    
}
