//
//  Constants.swift
//  Pokedex
//
//  Created by Mehdi Gilanpour on 7/4/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import Foundation

let URL_BASE = "http://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon/"

typealias downloadCompleted = () -> ()