//
//  DetailsVC.swift
//  Pokedex
//
//  Created by Mehdi Gilanpour on 7/4/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import UIKit

class DetailsVC: UIViewController {

    var pokemon : Pokemon!
    
    @IBOutlet weak var next: UIImageView!
    @IBOutlet weak var current: UIImageView!
    @IBOutlet weak var evo: UILabel!
    @IBOutlet weak var baseAttack: UILabel!
    @IBOutlet weak var weight: UILabel!
    @IBOutlet weak var height: UILabel!
    @IBOutlet weak var pokedexId: UILabel!
    @IBOutlet weak var defence: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var nameTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        nameTitle.text = pokemon.name.capitalizedString
        mainImage.image = UIImage(named: "\(pokemon.index)")
        next.image = UIImage(named: "\(pokemon.index)")
        current.image = UIImage(named: "\(pokemon.index)")

        pokemon.downloadPokemonDetails{()->()in
        
            print("FINISHED")
            self.caption.text = self.pokemon.Description
            self.weight.text = self.pokemon.weight
            self.height.text = self.pokemon.height
            self.baseAttack.text = self.pokemon.attack
            self.defence.text = self.pokemon.defense
            self.type.text = self.pokemon.type
            self.pokedexId.text = "\(self.pokemon.index)"
            if self.pokemon.nextEvolutionId == "" {
                self.evo.text = "No Evolution"
                self.next.hidden = true
            }else{
                self.next.hidden = false
                self.next.image = UIImage(named: self.pokemon.nextEvolutionId)
                
                var str = "Next Evolution: \(self.pokemon.nextEvolutionText)"
                
                if self.pokemon.nextEvolutionLevel != ""{
                    
                    str += "-Level: \(self.pokemon.nextEvolutionLevel)"
                }
                self.evo.text = str
            }
            
            
        }

    }

 
    @IBAction func back(sender: UIButton) {
        
        dismissViewControllerAnimated(true, completion:nil)
        
    }
    

}
