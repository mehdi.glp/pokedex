//
//  Pokemon.swift
//  Pokedex
//
//  Created by Mehdi Gilanpour on 7/3/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import Foundation
import Alamofire

class Pokemon {
    
    private var _name : String!
    private var _index : Int!
    private var _caption : String!
    private var _type : String!
    private var _defense : String!
    private var _height : String!
    private var _weight : String!
    private var _attack : String!
    private var _nextEevolutionId : String!
    private var _nextEvolutionText : String!
    private var _nextEvolutionLvl : String!
    private var _pokemonURL: String!
    
     var req : Alamofire.Request?
    
    var name : String{
        if _name == nil{
            _name = ""
        }
        return _name
    }
    
    var index : Int{
        if _index == nil{
            _index = 0
        }
        return _index
    }
    
    var Description : String{
        if _caption == nil{
            _caption = ""
        }
        return _caption
    }
    
    var type : String{
        if _type == nil{
            _type = ""
        }
        return _type
    }
    
    var nextEvolutionLevel : String{
        if _nextEvolutionLvl == nil{
            _nextEvolutionLvl = ""
        }
        return _nextEvolutionLvl
    }
    
    var nextEvolutionId : String{
        if _nextEevolutionId == nil{
            _nextEevolutionId = ""
        }
        return _nextEevolutionId
    }
    
    var nextEvolutionText : String{
        if _nextEvolutionText == nil {
            _nextEvolutionText = ""
        }
        return _nextEvolutionText
    }
    
    var weight : String{
        if _weight == nil{
            _weight = ""
        }
        return _weight
    }
    
    var height : String{
        if _height == nil{
            _height = ""
        }
        return _height
    }
    
    var attack : String{
        if _attack == nil{
            _attack = ""
        }
        return _attack
    }
    
    var defense : String{
        if _defense == nil {
            _defense = ""
        }
        return _defense
    }
    
    init(name:String,index:Int){
        self._index = index
        self._name = name
        
        self._pokemonURL = "\(URL_BASE)\(URL_POKEMON)\(self._index)"
    }
    
    func downloadPokemonDetails(completed:downloadCompleted){
       
        let url = NSURL(string: _pokemonURL)
        req = Alamofire.request(.GET, url!).responseJSON{response in
            
            let results = response.result
            if let dic = results.value as? Dictionary<String,AnyObject>{
                
                if let weight = dic["weight"] as? String{
                    self._weight = weight
                    print("w")
                    print(weight)
                }
                if let height = dic["height"] as? String{
                    self._height = height
                    print("h")
                    print(height)
                }
                if let attack = dic["attack"] as? Int{
                    self._attack = "\(attack)"
                    print("a")
                    print(attack)
                }
                if let defense = dic["defense"] as? Int{
                    self._defense = "\(defense)"
                    print("d")
                    print(defense)
                }
                
          
                
                
                if let evolutions = dic["evolutions"] as? [Dictionary<String ,AnyObject>] where evolutions.count > 0{
                    print("Sent")
                    if let to = evolutions[0]["to"] as? String{
                        //mega pokemons are not supported in this version
                        if to.rangeOfString("mega") == nil{
                            
                            if let str = evolutions[0]["resource_uri"] as? String {
                                let newStr = str.stringByReplacingOccurrencesOfString("/api/v1/pokemon/", withString: "")
                                let number = newStr.stringByReplacingOccurrencesOfString("/", withString: "")
                                self._nextEevolutionId = number
                                self._nextEvolutionText = to
                                
                                if let level = evolutions[0]["level"] as? Int{
                                    self._nextEvolutionLvl = "\(level)"
                                }
                                
                                print(self._nextEvolutionLvl)
                                print(self._nextEevolutionId)
                                print(self._nextEvolutionText)
                            }
                            
                        }
                    }
                    
                }
                
                if let types = dic["types"] as? [Dictionary<String,String>] where types.count > 0{
                    if let name = types[0]["name"]{
                        self._type = name.capitalizedString
                    }
                    if types.count > 1 {
                        for var x=1 ; x < types.count ; x++ {
                            if let name = types[x]["name"]{
                                self._type! += "/\(name.capitalizedString)"
                            }
                        }
                    }
                }else{
                    self._type = ""
                }
                
                print(self._type)
                
                
                if let descArr = dic["descriptions"] as? [Dictionary<String, String>] where descArr.count > 0 {
                    
                    if let url = descArr[0]["resource_uri"] {
                        let nsurl = NSURL(string: "\(URL_BASE)\(url)")!
                        
                        Alamofire.request(.GET, nsurl).responseJSON { response in
                            
                            let desResult = response.result
                            if let descDict = desResult.value as? Dictionary<String, AnyObject> {
                                
                                if let description = descDict["description"] as? String {
                                    self._caption = description
                                    print(self._caption)
                                }
                            }
                            
                            completed()
                        }
                    }
                    
                } else {
                    self._caption = ""
                }
                
                /*if let descArray = dic["descriptions"] as? [Dictionary<String,String>] where descArray.count > 0{
                    if let url = descArray[0]["resource_uri"]{
                        let nsurl = NSURL(string: "\(URL_BASE)\(url)")	
                        Alamofire.request(.GET, nsurl!).responseJSON{ respons in
                            print("request sent")
                            let descResult = response.result
                            //print(descResult.value)
                            if let descDic = descResult.value as? Dictionary<String,AnyObject>{
                                //print(descDic.values)
                                if let description = descDic["description"] as? String{
                                    self._caption = description
                                    print("FINAL"+"\(description)")
                                }
                            }
                            completed()
                        }
                        
                    }
                }else{
                    self._caption = ""
                }
                */
                
                
            }
        
        
        }
        
    }
    
    
    
}